const http = require("axios");

// 调用聚合接口获取生成八字
async function birthEight(birthday) {
  try {
    let onerow = birthday.split("\n")[0].split("：")[1];
    let tworow = birthday.split("\n")[1].split("：")[1];
    let year = onerow.split("-")[0];
    let month = onerow.split("-")[1];
    let day = onerow.split("-")[2].split(" ")[0];
    let hour = tworow;
    return await http.get(
      `http://apis.juhe.cn/birthEight/query?year=${year}&month=${month}&day=${day}&hour=${hour}&key=11216d571ab363057632180786c573e7`
    );
  } catch (error) {
    return {
      error: error,
      message: `您输入的日期格式有误，请按照如下模板重新输入\n\n阳历出生日期：2020-5-10\n出生时间(0~23)：5`,
    };
  }
}

module.exports = birthEight;
