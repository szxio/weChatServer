const express = require("express");
const app = express();
const path = require("path");
const weChat = require(path.resolve(__dirname, "./router/weChat"));
const xmlparser = require('express-xml-bodyparser');
app.use(express.json());
app.use(express.urlencoded());
app.use(xmlparser());

// 挂载并使用
app.use(weChat)

app.listen(8088, () => {
  console.log("服务已启动");
});
